import 'package:flutter/src/widgets/framework.dart';

class AppConstants {
  static final String FONT_FAMILY = 'Roboto';

  static final String METHOD_POST = 'POST';

  static final String googleApiKey = 'AIzaSyB-mj2kd8jE-tEArcKb32ESQoOHMUf5G7Y';

  /*Notification Extra*/

  /* recharge page */
  static final String oops = "oops something went wrong";

  /*report driver */

  static final String text_no_data_found = 'No data found.';
  static final String LOGIN_DETAILS = 'login_details';
  static final String DEVICE_ACCESS_ANDROID = "1";
  static final String DEVICE_ACCESS_IOS = "2";
  static final String NTITLE = "title";
  static final String NTYPE = 'nType';
  static final String NMESSAGE = "message";
  static final String NID = "id";
  static final String NSTATUS = "status";
  static final String ORDER_STATUS_PAID = "paid";
  static final String ORDER_STATUS_CANCELLED = "Cancelled";
  static final String ORDER_STATUS_COMPLETED = "Completed";
  static final String ORDER_STATUS_PENDING = "Pending";
  static final String ORDER_STATUS_CONFIRMED = "Confirmed";
  static final String ORDER_STATUS_RUNNING = "Running";

  /*Firebase chat constants*/
  static final String UID = "id";
  static final String USERS = "users";
  static final String NICKNAME = "nickname";
  static final String PHOTO_URL = "photoUrl";
  static final String CREATED_AT = "createdAt";
  static final String CHATTING_WITH = "chattingWith";
  static final String FRIENDS_IDS = "friendsIDs";

  static final String USER_ID = "userId";
  static final String LANG_ENGLISH = "en";
  static final String LANG_ARABIC = "ar";
  static final String LANG_KURDISH = "hi";
  static final String dummytext =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  static final String dummyCarImage =
      "https://auto.ndtvimg.com/car-images/big/kia/seltos/kia-seltos.jpg";
  static final String defaultImage =
      "https://x.kinja-static.com/assets/images/logos/placeholders/default.png";
  static final String NO_INTERNET_SPLASH =
      "Internet is not connected!, Please turn on the internet and re-open the app.";
  static final String home = "home";
  static final String addLocation = "addLocation";
  static final String paymentPage = "Payment Page";
  static final String bookingStatus = "bookingStatus";
  static final String bookingList = "bookingList";
  static final String notification = "notification";
  static final String dayType = "day";
  static final String weekType = "week";
  static final String monthType = "month";
  static final String bookingReview = "bookingReview";
}
