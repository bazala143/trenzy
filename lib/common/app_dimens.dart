import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppDimens {

  BuildContext mContext;
  AppDimens(this.mContext);

  void init(){
    ScreenUtil.instance = ScreenUtil(
        width: MediaQuery.of(mContext).size.width,
        height: MediaQuery.of(mContext).size.height)
      ..init(mContext);
  }

  static final double fontSizeExtraSmall = ScreenUtil(allowFontScaling: true).setSp(8);
  static final double fontSizeSmall = ScreenUtil(allowFontScaling: true).setSp(10);
  static final double fontSizeMedium = ScreenUtil(allowFontScaling: true).setSp(12);
  static final double fontSizeRegular = ScreenUtil(allowFontScaling: true).setSp(15);
  static final double fontSizeBig = ScreenUtil(allowFontScaling: true).setSp(18);
  static final double fontsizeExtraBig = ScreenUtil(allowFontScaling: true).setSp(20);
  static final double fontSizeVeryBig = ScreenUtil(allowFontScaling: true).setSp(25);
  static final double listVertcalPadding = ScreenUtil().setHeight(10);
  static final double listHorizontalPadding = ScreenUtil().setHeight(10);
}
