import 'package:shared_preferences/shared_preferences.dart';

import 'app_constant.dart';

class AppSettings {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  final String _kNotificationsPrefs = "allowNotifications";
  final String KEY_LOGIN_DETAILS = "KEY_LOGIN_DETAILS";
  final String KEY_CURRENCY_SYMBOL = "KEY_CURRENCY_SYMBOL";
  final String KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN";
  final String KEY_LANGUAGE_CODE = "KEY_LANGUAGE_CODE";
  final String KEY_FILTERS = "KEY_FILTERS";
  final String KEY_TOKEN_REGISTRED = "KEY_TOKEN_REGISTRED";

  /// ------------------------------------------------------------
  /// Method that returns the user decision to allow notifications
  /// ------------------------------------------------------------
  Future<bool> getAllowsNotifications() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_kNotificationsPrefs) ?? false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision to allow notifications
  /// ----------------------------------------------------------
  Future<bool> setAllowsNotifications(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(_kNotificationsPrefs, value);
  }

  /// ------------------------------------------------------------
  /// Method that returns the user decision on sorting order
  /// ------------------------------------------------------------
  Future<String> getLoginDetails() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(KEY_LOGIN_DETAILS) ?? null;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision on sorting order
  /// ----------------------------------------------------------
  Future<bool> setLoginDetails(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(KEY_LOGIN_DETAILS, value);
  }

  //////
  Future<String> getCurrencySymbol() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(KEY_CURRENCY_SYMBOL) ?? "\$";
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision on sorting order
  /// ----------------------------------------------------------
  Future<bool> setCurrencySybmol(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(KEY_CURRENCY_SYMBOL, value);
  }

  // Device Token
  Future<String> getDeviceToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(KEY_DEVICE_TOKEN) ?? "";
  }

  Future<bool> setDeviceToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(KEY_DEVICE_TOKEN, value);
  }

  // Language code
  Future<String> getLanguageCode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(KEY_LANGUAGE_CODE) ?? AppConstants.LANG_ENGLISH;
  }

  Future<bool> setLanguageCode(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(KEY_LANGUAGE_CODE, value);
  }

  // Filter
  Future<String> getFilters() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(KEY_FILTERS) ?? null;
  }

  Future<bool> setFilters(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(KEY_FILTERS, value);
  }

  Future<bool> isTokenRegistered() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(KEY_TOKEN_REGISTRED) ?? false;
  }

  Future<bool> setTokenRegistered(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(KEY_TOKEN_REGISTRED, value);
  }
}
