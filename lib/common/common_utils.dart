import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:connectivity/connectivity.dart';
import 'package:trenzy_ecommerce/widgets/my_custom_dialog.dart';


import 'common_colors.dart';

class CommonUtils {
  static bool isShowing = false;

  static void showErrorMessage(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text(message),
      duration: new Duration(seconds: 1),
    ));
  }

  static bool isEmpty(String string) {
    return string == null || string.length == 0;
  }

  static Widget getProgressBar() {
    return new Stack(
      children: [
        new Opacity(
          opacity: 0.2,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(
            strokeWidth: 2,
            backgroundColor: CommonColors.silver,
          ),
        ),
      ],
    );
  }

  static Widget getListItemProgressBar() {
    return new Container(
      color: Colors.white,
      padding: EdgeInsets.all(20.0),
      child: new Center(
        child: new CircularProgressIndicator(
          strokeWidth: 2,
          backgroundColor: CommonColors.silver,
        ),
      ),
    );
  }

  static void enableStatusBar(Color color) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: color));
  }

  static void hideStatusBar() {
    // To make this screen full screen.
    // It will hide status bar and notch.
    SystemChrome.setEnabledSystemUIOverlays([]);

    // full screen image for splash screen.
  }

  static Widget showProgressBar(Widget scafold) {
    return new Stack(
      children: <Widget>[
        scafold,
        new Container(
          color: Colors.black45,
          height: double.infinity,
          width: double.infinity,
          child: new Center(child: new CircularProgressIndicator()),
        )
      ],
    );
  }

  static bool isvalidEmail(String email) {
    bool emailValid =
        RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    return emailValid;
  }

  static void setPotraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  static Future<bool> checkInternetConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a mobile network.
      return true;
    } else {
      // Not connected to internet
      return false;
    }
  }

  static void showLongErrorMessage(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text(message),
      duration: new Duration(seconds: 4),
    ));
  }

  static void showProgressDialog(BuildContext context) {
    MyCustomDialog changePasswordDialog = MyCustomDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      //this right here
      child: Container(
        padding: EdgeInsets.all(10.0),
        alignment: Alignment.center,
        width: 200,
        height: 200,
        child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new CircularProgressIndicator(
                backgroundColor: CommonColors.grabck,
                strokeWidth: 2,
              ),
              new Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: new Text("Please wait...."))
            ],
          ),
        ),
      ),
    );

    if (!isShowing && context != null) {
      showMyDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return changePasswordDialog;
          });
      isShowing = true;
    }
  }

  static void hideProgressDialog(BuildContext context) {
    if (isShowing && context != null) {
      Navigator.of(context, rootNavigator: true).pop('dialog');
      isShowing = false;
    }
  }

  static bool isValidEmail(String isEmpty) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return regExp.hasMatch(isEmpty);
  }



  static bool isTab(BuildContext context){
    Size size = MediaQuery.of(context).size;
    double width = size.width > size.height ? size.height : size.width;
    if(width >= 600) {
      return true;
    } else {
      return false;
    }
  }
}
