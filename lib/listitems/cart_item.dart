import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/image_view.dart';

//  https://cdn.dribbble.com/users/1476727/screenshots/9169009/media/538c1f7930e6c94dd772de85cbce3926.png

class CartItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /*  ScreenUtil.instance = ScreenUtil(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height)
      ..init(context);*/

    AppDimens appDimens = new AppDimens(context);
    appDimens.init();
    var productImage = new Flexible(
      child: ProgressImageView(
        imageurl:
            "https://lcw.akinoncdn.com/products/2019/04/16/812579/5920adc2-b983-4c8b-87d2-2f216ae36ed0_size561x730.jpg",
        height: ScreenUtil().setHeight(150),
        width: ScreenUtil().setWidth(100),
      ),
      flex: 4,
    );

    var tvProductName = CTextView(
      text: "ShortCut for girls (Fit fro slim)",
      textColor: Colors.black,
      fontWeight: FontWeight.bold,
      maxLines: 2,
      textSize: ScreenUtil(allowFontScaling: true).setSp(16),
    );

    var tvProductId = CTextView(
      margin: EdgeInsets.only(top: ScreenUtil().setWidth(5.0)),
      text: "Piece ID : 1234564",
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
      textSize: ScreenUtil(allowFontScaling: true).setSp(12),
    );

    var tvProductSize = CTextView(
      text: "Size : Small",
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
      textSize: ScreenUtil(allowFontScaling: true).setSp(12),
    );

    var tvProductColors = CTextView(
      text: "Color : red",
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
      textSize: ScreenUtil(allowFontScaling: true).setSp(12),
    );

    var sizeIcon = new Container(
      alignment: Alignment.center,
      height: ScreenUtil().setHeight(20),
      width: ScreenUtil().setHeight(20),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(60),
          border: Border.all(color: Colors.black)),
      child: new CTextView(
        text: "S",
        textSize: ScreenUtil(allowFontScaling: true).setSp(15),
        textAlign: TextAlign.center,
      ),
    );

    var colorCode = new Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(5)),
      height: ScreenUtil().setHeight(20),
      width: ScreenUtil().setHeight(20),
      decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(60),
          border: Border.all(color: Colors.black)),
    );

    var variantRow = new Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(5.0)),
      child: new Row(
        children: <Widget>[
          tvProductSize,
          new Container(
            height: ScreenUtil().setHeight(10),
            width: 1,
            color: Colors.grey,
          ),
          tvProductColors
        ],
      ),
    );

    var variantIconRow = new Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(5.0)),
      child: new Row(
        children: <Widget>[sizeIcon, colorCode],
      ),
    );

    var iconAdd = InkWell(
      child: new Container(
        height: ScreenUtil().setHeight(20),
        width: ScreenUtil().setWidth(20),
        decoration: BoxDecoration(
          color: CommonColors.grabck,
          borderRadius: BorderRadius.circular(2.0)
        ),
        margin: EdgeInsets.only(right: ScreenUtil().setWidth(8)),
        child: new Icon(
          Icons.add,
          color: Colors.black,
          size: 15,
        ),
      ),
      onTap: () {},
    );

    var iconRemove = InkWell(
      child: new Container(
        height: ScreenUtil().setHeight(20),
        width: ScreenUtil().setWidth(20),
        decoration: BoxDecoration(
            color: CommonColors.grabck,
            borderRadius: BorderRadius.circular(2.0)
        ),
        margin: EdgeInsets.only(left: ScreenUtil().setHeight(8.0)),
        child: new Icon(
          Icons.remove,
          color: Colors.black,
          size: 15,
        ),
      ),
      onTap: () {},
    );

    var tvQty = CTextView(
      text: "20",
      textColor: Colors.black,
      fontWeight: FontWeight.bold,
      textSize: ScreenUtil(allowFontScaling: true).setSp(12),
    );

    var rowContainer = new Container(
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[iconAdd, tvQty, iconRemove],
      ),
    );

    var tvPrice = CTextView(
      text: "\$25",
      textColor: Colors.black,
      fontWeight: FontWeight.bold,
      textSize: ScreenUtil(allowFontScaling: true).setSp(20),
    );

    var priceRow = new Expanded(child: new Align(
      alignment: Alignment.bottomRight,
      child: new Container(
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[rowContainer, tvPrice],
        ),
      ),
    ));



    var detailColumn = Flexible(
      child: new Container(
        margin: EdgeInsets.only(left: ScreenUtil().setHeight(10)),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            tvProductName,
            tvProductId,
            variantRow,
            variantIconRow,
            priceRow
          ],
        ),
      ),
      flex: 10,
    );

    var mainRow = new Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[productImage, detailColumn],
    );
    return Container(
      height: ScreenUtil().setHeight(170),
      padding: EdgeInsets.only(
          top: ScreenUtil().setHeight(1),),
      child: Material(
        elevation: 2.0,
        child: new Container(
          padding: EdgeInsets.only(
              top: AppDimens.listVertcalPadding,
              bottom: AppDimens.listVertcalPadding,left: AppDimens.listHorizontalPadding,right: AppDimens.listHorizontalPadding),
          child: mainRow,
        ),
      ),
    );
  }
}
