import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/app_constant.dart';
import 'package:trenzy_ecommerce/model/menu_data.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';

class MenuItem extends StatelessWidget {
  final MenuData  menuData;
  final Function onPressed;


  MenuItem(this.menuData,this.onPressed);

  @override
  Widget build(BuildContext context) {
    final tvMenuName  = new CTextView(
      text: menuData.title ?? "",
      backgroundColor: Colors.transparent,
      fontWeight: FontWeight.normal,
      fontFamily: AppConstants.FONT_FAMILY,
      textColor: Colors.grey,
      textSize: 15,
    );

    final menuIcon = new Container(
      margin: EdgeInsets.only(left: 15,right: 10),
      child: new Icon(menuData.iconData,color: Colors.black,),
    );

    var lablName = menuData.menuLable == "" ? new Container() : Expanded(child:new Align(
      alignment: Alignment.centerRight,
      child:  new Container(
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            color: Colors.black,
            border: Border.all(color: Colors.black,width: 2),
            borderRadius: BorderRadius.circular(30)
        ),
        child: new CTextView(
          text:"20",
          textColor: Colors.white,
          textSize: 10,
          fontWeight: FontWeight.bold,
        ),
      ),
    ));
    return InkWell(
      onTap: onPressed,
      child: Container(
        margin: EdgeInsets.all(5.0),
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width - 50,
        color: Colors.white,
        height: 30,
        child: Row(
          children: <Widget>[menuIcon,tvMenuName,lablName],
        ),
      ),
    );
  }
}
