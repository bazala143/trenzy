import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';

class ProductFeature extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var tvFeatureName = Flexible(
      child: CTextView(
        margin: EdgeInsets.only(top: 5.0),
        text: "OS",
        fontWeight: FontWeight.normal,
        textSize: 14,
        textColor: Colors.grey,
      ),
      flex: 1,
    );

    var tvFeatureValue = Flexible(
      child: CTextView(
        margin: EdgeInsets.only(top: 5.0),
        text: "Android Pie",
        fontWeight: FontWeight.normal,
        textSize: 12,
        textColor: Colors.grey,
      ),
      flex: 1,
    );
    return Container(
      child: Row(
        children: <Widget>[tvFeatureName, tvFeatureValue],
      ),
    );
  }
}
