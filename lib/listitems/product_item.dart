import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/image_view.dart';

class ProductItem extends StatelessWidget {
  List<MainAxisAlignment> mainAxisList = [
    MainAxisAlignment.start,
    MainAxisAlignment.end,
    MainAxisAlignment.end
  ];

  Function onItemClicked;


  ProductItem({this.onItemClicked});

  @override
  Widget build(BuildContext context) {
    var imgProduct = ProgressImageView(
      scaleType: BoxFit.contain,
      imageurl:
          "https://lcw.akinoncdn.com/products/2019/04/16/812579/5920adc2-b983-4c8b-87d2-2f216ae36ed0_size561x730.jpg",
      height: ScreenUtil().setHeight(120),
      width: ScreenUtil().setWidth(180),
    );

    var trenzyVerified = new Container(
      margin: EdgeInsets.only(top: 10.0),
      padding: EdgeInsets.all(5.0),
      child: new Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Icon(
            Icons.verified_user,
            color: Colors.white,
            size: 12,
          ),
          CTextView(
            margin: EdgeInsets.only(left: 5.0),
            text: "Trenzy verified",
            textSize: 12,
            textColor: Colors.white,
            fontWeight: FontWeight.normal,
          )
        ],
      ),
      decoration: BoxDecoration(
          color: Colors.green, borderRadius: BorderRadius.circular(5.0)),
    );

    var tvProductPrice = CTextView(
      margin: EdgeInsets.only(right: 5.0),
      text: "\$50",
      textSize: 15,
      textColor: Colors.black,
      fontWeight: FontWeight.bold,
    );
    var tvOffPrice = new Align(
      alignment: Alignment.bottomRight,
      child: CTextView(
        text: "\$100",
        textSize: 10,
        textColor: Colors.grey,
        fontWeight: FontWeight.normal,
        textDecoration: TextDecoration.lineThrough,
      ),
    );

    var tvPrductName = CTextView(
      margin: EdgeInsets.only(top: 5.0),
      text: "Short black dress for girls(short cut)",
      textSize: 12,
      textAlign: TextAlign.justify,
      textColor: Colors.black87,
      fontWeight: FontWeight.normal,
    );

    var tvPrductSpecies = CTextView(
      margin: EdgeInsets.only(top: 2.0),
      text: "(xl,grey color)",
      textSize: 10,
      textAlign: TextAlign.start,
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
    );

    var tvReviewCount = CTextView(
      margin: EdgeInsets.only(top: 5.0),
      text: "(3226)",
      textSize: 10,
      textAlign: TextAlign.justify,
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
    );

    var favouriteIcon = new Align(
      alignment: Alignment.topRight,
      child: Container(
        child: Icon(
          Icons.favorite,
          color: Colors.grey,
          size: 18,
        ),
      ),
    );

    var productStack = new Container(
      height: ScreenUtil().setHeight(120),
      width: ScreenUtil().setWidth(180),
      child: new Stack(
        children: <Widget>[imgProduct, favouriteIcon],
      ),
    );

    var ratingBar = RatingBar(
      initialRating: 2.3,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemSize: 14,
      tapOnlyMode: false,
      ignoreGestures: true,
      itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.amber,
      ),
      onRatingUpdate: (rating) {
        print(rating);
      },
    );

    var container = new Container(
      height: 10,
      width: 10,
      decoration: BoxDecoration(
          color: Colors.blue, borderRadius: BorderRadius.circular(5)),
    );

    var container1 = new Container(
      margin: EdgeInsets.only(left: 5.0),
      height: 10,
      width: 10,
      decoration: BoxDecoration(
          color: Colors.red, borderRadius: BorderRadius.circular(5)),
    );

    var container2 = new Container(
      margin: EdgeInsets.only(left: 10.0),
      height: 10,
      width: 10,
      decoration: BoxDecoration(
          color: Colors.black, borderRadius: BorderRadius.circular(5)),
    );

    var stackColors = Stack(
      children: <Widget>[container, container1, container2],
    );

    var ratingRow = new Container(
      margin: EdgeInsets.only(top: 2.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: ratingBar,
            flex: 1,
            fit: FlexFit.tight,
          ),
          Flexible(
            child: tvReviewCount,
            flex: 1,
          )
        ],
      ),
    );

    var priceRow = new Container(
      margin: EdgeInsets.only(top: 5.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          stackColors,
          new Expanded(
              child: new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[tvProductPrice, tvOffPrice],
          ))
        ],
      ),
    );

    return InkWell(
      onTap: onItemClicked,
      child: new Container(
      padding: EdgeInsets.only(left: 5.0, bottom: 5.0),
      child: new Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(2.0),
        elevation: 2.0,
        child: new Container(
          margin: EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              productStack,
              tvPrductName,
              tvPrductSpecies,
              ratingRow,
              trenzyVerified,
              priceRow
            ],
          ),
        ),
      ),
    ),);
  }
}
