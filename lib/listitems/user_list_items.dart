import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';

class UserListItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var tvUserName = CTextView(
      text: "Kirit Chauhan",
      textSize: 18,
      textColor: Colors.black,
    );

    var tvAddress = CTextView(
      text: "Gokul Pura,Nr. Meldi mata temple,Marida,Marida Hathaj Road ,Nadiad ,Gujarat- 387002",
      textSize: 15,
      textColor: Colors.black,
    );

    var tvMobile = CTextView(
      text: "9724604987",
      textSize: 12,
      textColor: Colors.black,
    );

    var containerHome = Container(
      padding: EdgeInsets.only(left: 10, top: 5.0, bottom: 5.0, right: 10),
      decoration: BoxDecoration(color: CommonColors.light_gray),
      child: CTextView(
        text: "Home",
        textSize: 12,
        textColor: Colors.black,
      ),
    );

    var homeRow = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[tvUserName, containerHome],
      ),
    );

    var mainColumn = Column(
      children: <Widget>[homeRow, tvAddress, tvMobile],
    );

    return Container(
      child: Material(
        elevation: 5,
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: EdgeInsets.all(10),
          child: mainColumn,
        ),
      ),
    );
  }
}
