import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/image_view.dart';

class WishlistItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var productImage = new Flexible(
      child: ProgressImageView(
        imageurl:
        "https://lcw.akinoncdn.com/products/2019/04/16/812579/5920adc2-b983-4c8b-87d2-2f216ae36ed0_size561x730.jpg",
        height: ScreenUtil().setHeight(150),
        width: ScreenUtil().setWidth(100),
      ),
      flex: 4,
    );

    var tvProductName = CTextView(
      text: "ShortCut for girls (Fit fro slim)",
      textColor: Colors.black,
      fontWeight: FontWeight.bold,
      maxLines: 2,
      textSize: ScreenUtil(allowFontScaling: true).setSp(16),
    );

    var tvProductId = CTextView(
      margin: EdgeInsets.only(top: ScreenUtil().setWidth(5.0)),
      text: "Piece ID : 1234564",
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
      textSize: ScreenUtil(allowFontScaling: true).setSp(12),
    );

    var tvProductSize = CTextView(
      text: "Size : Small",
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
      textSize: ScreenUtil(allowFontScaling: true).setSp(12),
    );

    var tvProductColors = CTextView(
      text: "Color : red",
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
      textSize: ScreenUtil(allowFontScaling: true).setSp(12),
    );


    var variantRow = new Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(5.0)),
      child: new Row(
        children: <Widget>[
          tvProductSize,
          new Container(
            height: ScreenUtil().setHeight(10),
            width: 1,
            color: Colors.grey,
          ),
          tvProductColors
        ],
      ),
    );

   var btnAddToBag = new InkWell(
     child: new Container(
       alignment: Alignment.center,
       width: 100,
       height: ScreenUtil().setHeight(30),
       decoration: BoxDecoration(
         color: CommonColors.primaryColor,
       ),
       child: new CTextView(
         text: "ADD TO CART",
         textColor: Colors.white,
         textSize: 12,
         fontWeight: FontWeight.bold,
       ),
     ),

   );



    var tvPrice = CTextView(
      text: "\$25",
      textColor: Colors.black,
      fontWeight: FontWeight.bold,
      textSize: ScreenUtil(allowFontScaling: true).setSp(20),
    );

    var priceRow = new Expanded(child: new Align(
      alignment: Alignment.bottomRight,
      child: new Container(
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[tvPrice,btnAddToBag],
        ),
      ),
    ));



    var detailColumn = Flexible(
      child: new Container(
        margin: EdgeInsets.only(left: ScreenUtil().setHeight(10)),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            tvProductName,
            tvProductId,
            variantRow,
            priceRow
          ],
        ),
      ),
      flex: 10,
    );

    var mainRow = new Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[productImage, detailColumn],
    );
    return Slidable(child: Container(
      height: ScreenUtil().setHeight(170),
      padding: EdgeInsets.only(
        top: ScreenUtil().setHeight(1),),
      child: Material(
        elevation: 2.0,
        child: new Container(
          padding: EdgeInsets.only(
              top: AppDimens.listVertcalPadding,
              bottom: AppDimens.listVertcalPadding,left: AppDimens.listHorizontalPadding,right: AppDimens.listHorizontalPadding),
          child: mainRow,
        ),
      ),
    ), actionPane: SlidableDrawerActionPane(),actionExtentRatio: 0.25,secondaryActions: <Widget>[new Container(
      color: CommonColors.grabck,
      height:ScreenUtil().setHeight(170) ,
      child: Icon(Icons.delete_forever,color: Colors.red,),
    )],);
  }
}
