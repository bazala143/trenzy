import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trenzy_ecommerce/views/cart_page.dart';
import 'package:trenzy_ecommerce/views/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

   int _blackPrimaryValue = 0xFF000000;


  final routes = <String, WidgetBuilder>{
    HomePage.tag: (context) => HomePage(),
    CartPage.tag: (context) => CartPage(),

  };



  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
      Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'PSHOP',
      theme: ThemeData(
        primarySwatch:MaterialColor(
          _blackPrimaryValue,
          <int, Color>{
            50: Color(0xFF000000),
            100: Color(0xFF000000),
            200: Color(0xFF000000),
            300: Color(0xFF000000),
            400: Color(0xFF000000),
            500: Color(_blackPrimaryValue),
            600: Color(0xFF000000),
            700: Color(0xFF000000),
            800: Color(0xFF000000),
            900: Color(0xFF000000),
          },
        ),
      ),
      home: HomePage(),
    );
  }
}

