
import 'package:flutter/material.dart';

class MenuData{
  IconData iconData;
  String   title;
  String menuLable;
  MenuData(this.iconData, this.title,this.menuLable);
}