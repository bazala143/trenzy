import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';

class ShimmerView extends StatelessWidget {
  final double height, width,radious;
  EdgeInsets margin;


  ShimmerView({this.height, this.width,this.radious,this.margin});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: CommonColors.grabck,
      highlightColor: CommonColors.silver,
      child: new Container(
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(radious!= null ? radious : 1.0),
        ),
        margin: margin ?? EdgeInsets.all(10.0),
        height: height,
        width: width,

      ),
    );
  }
}
