import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/shimmer_views/ShimmerView.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/image_view.dart';

class ProductItemShimmer extends StatelessWidget {

  List<MainAxisAlignment> mainAxisList = [MainAxisAlignment.start,MainAxisAlignment.end,MainAxisAlignment.end];
  @override
  Widget build(BuildContext context) {
    var imgProduct = ShimmerView(
      margin: EdgeInsets.all(0.0),
      radious: 2,
      height: ScreenUtil().setHeight(160),
      width: double.infinity,
    );

    var tvProductPrice = new ShimmerView(
      margin: EdgeInsets.all(0.0),
      radious: 2,
      height: 10,
      width: 50,
    );
    var tvOffPrice =  new ShimmerView(
      radious: 2,
      height: 10,
      width: 40,
    );

    var tvPrductName = new ShimmerView(
      margin: EdgeInsets.only(top: 5.0),
      radious: 2,
      height: 12,
      width: 170,
    );

    var tvPrductSpecies =  new ShimmerView(
      margin: EdgeInsets.only(top: 5.0),
      radious: 2,
      height: 10,
      width: 100,
    );

    var tvReviewCount = new ShimmerView(
      radious: 2,
      height: 10,
      width: 50,
    );

    var favouriteIcon = new Align(
      alignment: Alignment.topRight,
      child: new ShimmerView(
        radious: 10,
        height: 20,
        width: 20,
      ),
    );

    var productStack = new Container(
      height: ScreenUtil().setHeight(160),
      width: double.infinity,
      child: new Stack(
        children: <Widget>[imgProduct, favouriteIcon],
      ),
    );

    var ratingBar =  RatingBar(
      initialRating: 0.0,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemSize: 12,
      tapOnlyMode: false,
      ignoreGestures: true,
      itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.amber,
      ),
      onRatingUpdate: (rating) {
        print(rating);
      },
    );

    var container =new ShimmerView(
      height: 10,
      width: 10,
      radious: 10,
      margin: EdgeInsets.only(left:5.0),

    );

    var container1 =new ShimmerView(
      margin: EdgeInsets.only(left: 5.0),
      radious: 10,
      height: 10,
      width: 10,);

    var container2 =new ShimmerView(
      margin: EdgeInsets.only(left: 10.0),
      height: 10,
      width: 10,
      radious: 10,


    );

    var stackColors = Container(
      child: Stack(
        children: <Widget>[
          container,container1,container2
        ],
      ),
    );

    var ratingRow = new Container(
      margin: EdgeInsets.only(top: 2.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[Flexible(child: ratingBar,flex: 1,fit: FlexFit.tight,),Flexible(child: tvReviewCount,flex: 1,)],
      ),
    );

    var priceRow = new Container(
      margin: EdgeInsets.only(top: 5.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[stackColors,new Expanded(child: new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[tvProductPrice,tvOffPrice],
        ))],
      ),
    );

    return new Container(
      padding: EdgeInsets.only(left: 5.0,bottom: 5.0),
      child: new Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(2.0),
        elevation: 2.0,
        child: new Container(
          padding: EdgeInsets.only(left: 10.0,right: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[productStack,tvPrductName,tvPrductSpecies, ratingRow, priceRow],
          ),
        ),
      ),);
  }
}
