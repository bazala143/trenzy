import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'package:trenzy_ecommerce/common/custom_icons.dart';
import 'package:trenzy_ecommerce/listitems/cart_item.dart';
import 'package:trenzy_ecommerce/widgets/c_scaffold.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/flat_button.dart';

class CartPage extends StatefulWidget {
  static String tag = "cart-page";

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    AppDimens appDimens = new AppDimens(context);
    appDimens.init();
    var cartListview = AnimationLimiter(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: 2,
        itemBuilder: (BuildContext context, int index) {
          return AnimationConfiguration.staggeredList(
            position: index,
            duration: const Duration(milliseconds: 375),
            child: SlideAnimation(
              verticalOffset: 50.0,
              child: FadeInAnimation(
                child: CartItem(),
              ),
            ),
          );
        },
      ),
    );
    
    var tvUsePromoCode = CTextView(
      text: "Use Promocode",
      textSize: AppDimens.fontSizeRegular,
      textColor: Colors.grey,
      fontWeight: FontWeight.w600,
    );
    
    var navIcon = new Icon(Icons.navigate_next,color: Colors.grey,);
    
    
    var promocodeLayout = new Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(1)),
      height: ScreenUtil().setHeight(50),
      child: Material(
        elevation: 1.0,
        child: new Container(
          padding: EdgeInsets.only(left: AppDimens.listHorizontalPadding,right: AppDimens.listHorizontalPadding),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[tvUsePromoCode,navIcon],
          ),
        ),
      ),
    );

    var tvTotalPrice = CTextView(
      text: "Total",
      textSize: AppDimens.fontSizeRegular,
      textColor: Colors.grey,
      fontWeight: FontWeight.w600,
    );

    var tvPrice = CTextView(
      text: "\$50",
      textSize: AppDimens.fontSizeBig,
      textColor: Colors.black,
      fontWeight: FontWeight.bold,

    );

    var totalPrice = new Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(1)),
      height: ScreenUtil().setHeight(50),
      child: Material(
        elevation: 1.0,
        child: new Container(
          padding: EdgeInsets.only(left: AppDimens.listVertcalPadding,right: AppDimens.listVertcalPadding),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[tvTotalPrice,tvPrice],
          ),
        ),
      ),
    );

    return CScaffold(
      bottomNavBar: RectButton(
        backgroundColor: Colors.black,
        margin: EdgeInsets.only(left:ScreenUtil().setWidth(5),right: ScreenUtil().setWidth(5)),
        elevation: 2.0,
        btnText: "Buy",
      ),
      backgroundColor: Colors.white,
      leadingIcon: InkWell(
        child: Icon(Icons.close,color: Colors.black,),
        onTap: (){
          Navigator.pop(context);
        },
      ),
      elevation: 0.0,
      actions: <Widget>[new Container(
        margin: EdgeInsets.only(right: ScreenUtil().setWidth(10)),
        child: new Icon(CustomIcons.edit_alt,color: Colors.black,),)],
      title: "2 Items",
      isLeading: true,
      body:ListView(
        shrinkWrap: true,
        children: <Widget>[
          cartListview,promocodeLayout,totalPrice
        ],

      ),
    );
  }
}
