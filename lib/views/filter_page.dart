import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/widgets/c_scaffold.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:trenzy_ecommerce/widgets/range_slider.dart';

class FilterScreen extends StatefulWidget {
  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  RangeValues _values = RangeValues(10, 1000);
  double distValue = 50.0;
  List<Color> colorList = [
    Colors.white,
    Colors.black,
    Colors.blue,
    Colors.green,
    Colors.lightBlue,
    Colors.blueAccent,
    Colors.brown
  ];
  List<String> discountList = ["5%", "10%", "20%", "25%"];
  List<String>  brandList =["Samsung","Nokia","Sony","Vivo","oppo","Gionee","Xolo","Apple","Motorola","Mi"];
  List<String>  categoryList =["Mobile","Television","Laptop","Desktop","Appliances","Shoes","Clothes","Appreals","Jwellery","Auto"];

  @override
  Widget build(BuildContext context) {
    var tvApply = new Align(
      alignment: Alignment.center,
      child: new CTextView(
        margin: EdgeInsets.only(right: 10.0),
        text: "Apply",
        textSize: 12,
        textColor: Colors.black,
        fontWeight: FontWeight.bold,
      ),
    );

    var tvPrice = new Align(
      alignment: Alignment.centerLeft,
      child: new CTextView(
        padding:
            EdgeInsets.only(right: 10.0, top: 10.0, left: 20.0, bottom: 10.0),
        text: "Price",
        textSize: 15,
        textColor: Colors.grey,
        fontWeight: FontWeight.w400,
      ),
    );

    var rangeSlider = RangeSliderView(
      values: _values,
      onChnageRangeValues: (values) {
        _values = values;
      },
    );

    var tvCategory = new Align(
      alignment: Alignment.centerLeft,
      child: new CTextView(
        padding:
        EdgeInsets.only(right: 10.0, top: 10.0, left: 20.0, bottom: 10.0),
        text: "Category",
        textSize: 15,
        textColor: Colors.grey,
        fontWeight: FontWeight.w400,
      ),
    );

    var categoryGridview = new Container(
      child: GridView.count(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        childAspectRatio: 2.5,
        crossAxisCount: 4,
        children: List.generate(categoryList.length, (index) {
          return new Align(
            alignment: Alignment.center,
            child: new Container(
              margin: EdgeInsets.only(left: 5.0, right: 5.0),
              alignment: Alignment.center,
              height: 30,
              padding:
              EdgeInsets.only(left: 10, right: 10, top: 5.0, bottom: 5.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: CTextView(
                text: categoryList[index],
                textColor: Colors.black,
                textSize: 12,
              ),
            ),
          );
        }),
      ),
    );

    var tvBrand = new Align(
      alignment: Alignment.centerLeft,
      child: new CTextView(
        padding:
            EdgeInsets.only(right: 10.0, top: 10.0, left: 20.0, bottom: 10.0),
        text: "Brand",
        textSize: 15,
        textColor: Colors.grey,
        fontWeight: FontWeight.w400,
      ),
    );

    var brandGridview = new Container(
      child: GridView.count(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        childAspectRatio: 2.5,
        crossAxisCount: 4,
        children: List.generate(brandList.length, (index) {
          return new Align(
            alignment: Alignment.center,
            child: new Container(
              margin: EdgeInsets.only(left: 5.0, right: 5.0),
              alignment: Alignment.center,
              height: 30,
              padding:
                  EdgeInsets.only(left: 10, right: 10, top: 5.0, bottom: 5.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: CTextView(
                text: brandList[index],
                textColor: Colors.black,
                textSize: 12,
              ),
            ),
          );
        }),
      ),
    );

    var tvColor = new Align(
      alignment: Alignment.centerLeft,
      child: new CTextView(
        padding:
            EdgeInsets.only(right: 10.0, top: 10.0, left: 20.0, bottom: 10.0),
        text: "Color",
        textSize: 15,
        textColor: Colors.grey,
        fontWeight: FontWeight.w400,
      ),
    );

    var colorGridview = new Container(
      child: GridView.count(
        padding: EdgeInsets.only(left: 10.0, right: 10.0,bottom: 10.0),
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        childAspectRatio: 1.5,
        crossAxisCount: 7,
        children: List.generate(colorList.length, (index) {
          return new Align(
            alignment: Alignment.center,
            child: new Container(
              height: 30,
              width: 30,
              margin: EdgeInsets.only(left: 2.0, right: 2.0),
              alignment: Alignment.center,
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  color: colorList[index],
                  border: Border.all(color: Colors.grey, width: 1),
                  borderRadius: BorderRadius.circular(30)),
            ),
          );
        }),
      ),
    );

    var tvDiscount = new Align(
      alignment: Alignment.centerLeft,
      child: new CTextView(
        padding:
            EdgeInsets.only(right: 10.0, top: 10.0, left: 20.0),
        text: "Discount",
        textSize: 15,
        textColor: Colors.grey,
        fontWeight: FontWeight.w400,
      ),
    );

    var discountGridview = new Container(
      child: GridView.count(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        childAspectRatio: 1.5,
        crossAxisCount: 4,
        children: List.generate(discountList.length, (index) {
          return new Align(
            alignment: Alignment.center,
            child: new Container(
              margin: EdgeInsets.only(left: 5.0, right: 5.0),
              alignment: Alignment.center,
              height: 30,
              padding:
                  EdgeInsets.only(left: 10, right: 10, top: 5.0, bottom: 5.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.black, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: CTextView(
                text: index == 9 ? "More" : discountList[index],
                textColor: Colors.black,
                textSize: 12,
              ),
            ),
          );
        }),
      ),
    );

    var tvCustomerRating = new Align(
      alignment: Alignment.centerLeft,
      child: new CTextView(
        padding:
        EdgeInsets.only(right: 10.0, top: 10.0, left: 20.0),
        text: "Customer Ratings",
        textSize: 15,
        textColor: Colors.grey,
        fontWeight: FontWeight.w400,
      ),
    );

    var ratingsGridview = new Container(
      child: GridView.count(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        childAspectRatio: 4.0,
        crossAxisCount: 2,
        children: List.generate(4, (index) {
          return new Align(
            alignment: Alignment.center,
            child: new Container(
              margin: EdgeInsets.only(left: 5.0, right: 5.0),
              alignment: Alignment.center,
              height: 30,
              padding:
              EdgeInsets.only(left: 10, right: 10, top: 5.0, bottom: 5.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.black, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Row(
                children: <Widget>[CTextView(text: "4 ",textSize: 12,textColor: Colors.black,),Icon(Icons.star,size: 12,),CTextView(text: " & above",textSize: 12,textColor: Colors.black,)],
              ),
            ),
          );
        }),
      ),
    );

    var divider = new Container(
      margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      width: double.infinity,
      height: 1,
      color: CommonColors.grabck,
    );

    var mainListview = ListView(
      shrinkWrap: true,
      children: <Widget>[
        tvPrice,
        rangeSlider,
        divider,
        tvCategory,
        categoryGridview,
        divider,
        tvBrand,
        brandGridview,
        divider,
        tvColor,
        colorGridview,
        divider,
        tvDiscount,
        discountGridview,divider,tvCustomerRating,ratingsGridview
      ],
    );

    return CScaffold(
      title: "Filter",
      elevation: 1.0,
      leadingIcon: Icon(
        Icons.close,
        color: Colors.black,
      ),
      actions: <Widget>[tvApply],
      body: mainListview,
    );
  }
}
