import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/listitems/product_item.dart';
import 'package:trenzy_ecommerce/shimmer_views/product_list_shimmers.dart';
import 'package:trenzy_ecommerce/views/product_list_page.dart';
import 'package:trenzy_ecommerce/views/product_detail_page.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/image_view.dart';
import 'package:trenzy_ecommerce/widgets/search_field.dart';

class HomeFragment extends StatefulWidget {
  @override
  _HomeFragmentState createState() => _HomeFragmentState();
}

class _HomeFragmentState extends State<HomeFragment> {
  List<String> bannerList = new List();
  List<String> categoryList = new List();
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bannerList.add(
        "https://image.freepik.com/free-vector/sale-banners-set_9620-111.jpg");
    bannerList.add(
        "https://image.freepik.com/free-vector/modern-super-sale-promotion-bright-banner_1055-6985.jpg");
    bannerList.add(
        "https://image.freepik.com/free-vector/modern-cyber-monday-sale-banner_1048-11424.jpg");

    categoryList.add(
        "https://static.digit.in/default/7d958aba4b668c483874d7aac8b3f49b6f9e7b4a.jpeg");
    categoryList.add(
        "https://www.volusion.com/blog/content/images/2017/04/The-Ultimate-Guide-to-Selling-Clothes---Other-Apparel-Online.jpg");
    categoryList.add(
        "https://cdn.pixabay.com/photo/2014/05/02/21/49/home-office-336373_960_720.jpg");
    categoryList.add(
        "https://rukminim1.flixcart.com/image/416/416/k0mqtu80/headphone/v/v/x/noise-shots-x1-air-truly-wireless-original-imafkdxhndtjazth.jpeg");
    categoryList.add(
        "https://image.freepik.com/free-vector/modern-super-sale-promotion-bright-banner_1055-6985.jpg");
    categoryList.add(
        "https://image.freepik.com/free-vector/modern-cyber-monday-sale-banner_1048-11424.jpg");

    Future.delayed(Duration(seconds: 3),(){
      setState(() {
       isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var iconSerach = new Container(
      margin: EdgeInsets.only(left: 10.0),
      child: Icon(
        Icons.search,
        color: Colors.grey,
      ),
    );
    var tvSearch = CTextView(
      margin: EdgeInsets.only(left: 5.0),
      textSize: AppDimens.fontSizeRegular,
      text: "Search product here...",
      textColor: Colors.grey,
    );

    var serachRow = new Row(
      children: <Widget>[iconSerach, tvSearch],
    );

    var searchBar = new Container(
      margin: EdgeInsets.only(left: 5, right: 5, top: 10, bottom: 10),
      height: 40,
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: CommonColors.grabck,
      ),
      child: serachRow,
    );

    var slider = CarouselSlider(
      height: 150.0,
      items: List.generate(bannerList.length, (index) {
        return new ProgressImageView(
          borderRadius: ScreenUtil().setWidth(10.0),
          margin: EdgeInsets.only(left: ScreenUtil().setWidth(5.0)),
          height: 150,
          width: double.infinity,
          imageurl: bannerList[index],
        );
      }),
    );

    var gridView = GridView.count(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      padding: EdgeInsets.only(
          top: AppDimens.listVertcalPadding,
          left: AppDimens.listHorizontalPadding,
          right: AppDimens.listHorizontalPadding),
      crossAxisCount: 2,
      childAspectRatio: 0.63,
      children: List.generate(4, (int index) {
        return isLoading ? ProductItemShimmer() : ProductItem(onItemClicked: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ProductDetailPage()),
          );
        },);
      }),
    );

    var categoryListView = new Container(
      height: 50,
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: categoryList.length,
        itemBuilder: (BuildContext context, int index) {
          return new Align(
            alignment: Alignment.center,
            child: new Container(
              margin: EdgeInsets.only(left: 10),
              child: Stack(
                children: <Widget>[new ProgressImageView(
                  scaleType: BoxFit.cover,
                  borderRadius: 10.0,
                  elevation: 5.0,
                  height: 40,
                  width: 40,
                  imageurl: categoryList[index],
                ),new Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius: BorderRadius.circular(10.0)
                  ),
                )],
              ),
            ),
          );
        },
      ),
    );

    var mainListview = new ListView(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      children: <Widget>[
        searchBar,
        slider,
        getListLableWidget("Top Categories"),
        categoryListView,getListLableWidget("Today's deal"),gridView
      ],
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: mainListview,
    );
  }

  Widget getListLableWidget(String name) {
    var tvTopCategories = CTextView(
      text: name,
      textSize: AppDimens.fontsizeExtraBig,
      fontWeight: FontWeight.bold,
      textColor: Colors.black,
    );

    var tvShowAll = InkWell(
      onTap: (){
        _redirectToPage(ProductListPage(), context);
      },
      child: CTextView(
        text: "Show All",
        textSize: AppDimens.fontSizeMedium,
        fontWeight: FontWeight.normal,
        textColor: Colors.grey,
      ),
    );

    var categoryRow = new Container(
      margin: EdgeInsets.only(
          left: AppDimens.listHorizontalPadding,
          right: AppDimens.listHorizontalPadding,
          top: 10.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[tvTopCategories, tvShowAll],
      ),
    );
    return categoryRow;
  }

  _redirectToPage(Widget page, BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => page),
    );
  }
}
