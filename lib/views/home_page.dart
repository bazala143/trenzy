import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'package:trenzy_ecommerce/listitems/menu_items.dart';
import 'package:trenzy_ecommerce/model/menu_data.dart';
import 'package:trenzy_ecommerce/views/home_fragment.dart';
import 'package:trenzy_ecommerce/views/login_view.dart';
import 'package:trenzy_ecommerce/views/product_list_page.dart';
import 'package:trenzy_ecommerce/views/profile_view.dart';
import 'package:trenzy_ecommerce/views/user_order_list.dart';
import 'package:trenzy_ecommerce/views/wishlist_fragment.dart';
import 'package:trenzy_ecommerce/widgets/circule_imageview.dart';
import 'package:trenzy_ecommerce/widgets/c_scaffold.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';

import 'cart_page.dart';
import 'filter_page.dart';

class HomePage extends StatefulWidget {
  static String tag = "home-page";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<MenuData> menuList = new List();
  List<MenuData> drawerList = new List();
  Widget homeBody = UserOrderList();
  String title = "PSHOP";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    MenuData menuData1 = new MenuData(Icons.home, "Home", "");
    MenuData menuData2 = new MenuData(Icons.add_shopping_cart, "CarList", "2");
    MenuData menuData3 = new MenuData(Icons.favorite_border, "Favourite", "3");
    MenuData menuData4 = new MenuData(Icons.bookmark_border, "Orders", "10");
    MenuData menuData9 = new MenuData(Icons.local_offer, "offers", "");
    MenuData menuData5 = new MenuData(Icons.help, "Help", "");
    MenuData menuData8 = new MenuData(Icons.person_outline, "Profile", "");
    menuList.add(menuData1);
    menuList.add(menuData2);
    menuList.add(menuData3);
    menuList.add(menuData4);
    menuList.add(menuData5);
    MenuData menuData6 = new MenuData(Icons.settings, "Setting", "");
    MenuData menuData7 = new MenuData(Icons.exit_to_app, "Logout", "");
    drawerList.add(menuData1);
    drawerList.add(menuData2);
    drawerList.add(menuData3);
    drawerList.add(menuData4);
    drawerList.add(menuData9);
    drawerList.add(menuData6);
    drawerList.add(menuData8);
    drawerList.add(menuData7);
    drawerList.add(menuData5);
  }

  @override
  Widget build(BuildContext context) {
    AppDimens appDimens = new AppDimens(context);
    appDimens.init();

    var homeIcon = Flexible(
      child: InkWell(
        child: Icon(
          Icons.home,
          color: Colors.black,
        ),
        onTap: () {
          setState(() {
            homeBody = HomeFragment();
            title = "PSHOP";
          });
        },
      ),
      flex: 1,
      fit: FlexFit.tight,
    );
    var favIcon = Flexible(
      child: InkWell(
        onTap: () {
          setState(() {
            homeBody = WishListFragment();
            title = "WishList";
          });
        },
        child: Icon(
          Icons.favorite_border,
          color: Colors.black,
        ),
      ),
      flex: 1,
      fit: FlexFit.tight,
    );
    var orderIcon = Flexible(
      child: InkWell(
        child: Icon(
          Icons.bookmark_border,
          color: Colors.black,
        ),
        onTap: () {
          setState(() {
            homeBody = UserOrderList();
            title = "Orders";
          });
        },
      ),
      flex: 1,
      fit: FlexFit.tight,
    );
    var profileIcon = Flexible(
      child: InkWell(
        onTap: () {
          setState(() {
            title = "Profile";
            homeBody = ProfileView();
          });
        },
        child: Icon(
          Icons.person_outline,
          color: Colors.black,
        ),
      ),
      flex: 1,
      fit: FlexFit.tight,
    );

    var bottomRow = new Row(
      children: <Widget>[homeIcon, favIcon, orderIcon, profileIcon],
    );

    var tvNotCount = new Align(
      child: new Container(
        margin: EdgeInsets.only(top: 8.0, right: 3.0),
        alignment: Alignment.center,
        height: ScreenUtil().setHeight(15),
        width: ScreenUtil().setWidth(15),
        decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(ScreenUtil().setHeight(60))),
        child: CTextView(
          textSize: AppDimens.fontSizeExtraSmall,
          text: "99",
          textColor: Colors.white,
        ),
      ),
      alignment: Alignment.topRight,
    );

    var stackNotification = new Container(
      height: ScreenUtil().setHeight(30),
      width: ScreenUtil().setWidth(30),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          new Container(
            height: ScreenUtil().setHeight(30),
            width: ScreenUtil().setWidth(30),
            child: Icon(
              Icons.notifications_none,
              color: Colors.black,
            ),
          ),
          tvNotCount
        ],
      ),
    );

    var drawerProfileImage = new Align(
      alignment: Alignment.center,
      child: CircleImageView(
        height: 100,
        width: 100,
        margin: EdgeInsets.only(top: 50),
        imageUrl:
            "https://static-ssl.businessinsider.com/image/59f8dc483e9d25db458b5dfc-2400/gettyimages-645671866.jpg",
      ),
    );

    var tvUserName = CTextView(
      margin: EdgeInsets.only(top: 10.0),
      text: "Elon Musk",
      textColor: Colors.grey,
      textAlign: TextAlign.center,
      textSize: 15,
      fontWeight: FontWeight.bold,
    );

    var drawerListview = new ListView.builder(
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return MenuItem(drawerList[index], () {
          if (index == 0) {
            setState(() {
              homeBody = HomeFragment();
              title = "PSHOP";
            });
          } else if (index == 1) {
            Navigator.pop(context);
            _redirectToPage(CartPage(), context);
          } else if (index == 2) {
            setState(() {
              homeBody = WishListFragment();
              title = "WishList";
            });
          } else if (index == 6) {
            setState(() {
              homeBody = ProfileView();
              title = "Profile";
            });
          } else if (index == 7) {
            _redirectToPage(LoginView(), context);
          }
        });
      },
      itemCount: drawerList.length,
    );

    return CScaffold(
      body: homeBody,
      drawer: new Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width - 100,
        height: MediaQuery.of(context).size.height,
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.all(0.0),
          children: <Widget>[drawerProfileImage, tvUserName, drawerListview],
        ),
      ),
      bottomNavBar: bottomRow,
      title: title,
      actions: <Widget>[
        new Container(
          margin: EdgeInsets.only(right: 5.0),
          child: Icon(
            Icons.shopping_cart,
            color: Colors.black,
          ),
        ),
        stackNotification
      ],
      leadingIcon: new Container(
        height: 20,
        width: 20,
        child: ClipRRect(
          clipBehavior: Clip.antiAlias,
          child: Icon(
            Icons.menu,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  _redirectToPage(Widget page, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => page),
    );
  }
}
