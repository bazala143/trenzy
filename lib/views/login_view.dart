import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/widgets/c_scaffold.dart';
import 'package:trenzy_ecommerce/widgets/c_textfield.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/flat_button.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  TextEditingController userNameEditingController = new TextEditingController();
  TextEditingController userPasswordEditingController =
      new TextEditingController();
  bool isPasswordVisible = true;

  @override
  Widget build(BuildContext context) {
    var etUserName = CTextField(
      margin: EdgeInsets.all(10.0),
      textEditingController: userNameEditingController,
      hint: "UserName",
      maxLines: 1,
    );

    var etPassword = CTextField(
      margin: EdgeInsets.all(10.0),
      textEditingController: userPasswordEditingController,
      hint: "Password",
      maxLines: 1,
      obscureText: isPasswordVisible,
      onShowHide: (){
        setState(() {
          isPasswordVisible = !isPasswordVisible;
        });
      },
    );

    var forgotPassword = Align(
      alignment: Alignment.centerRight,
      child: CTextView(
        margin: EdgeInsets.only(top: 10.0,right: 10.0),
        text: "Forgot Password ?",
        textColor: CommonColors.black,
        textSize: 18,
      ),
    );

    var loginButton = RectButton(
      margin: EdgeInsets.only(top: 20.0),
      onPressed: () {},
      btnText: "Login",
      elevation: 5.0,
    );

    var column = new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[etUserName, etPassword,forgotPassword, loginButton],
    );

    return CScaffold(
      elevation: 0.0,
      title: "Login",
      leadingIcon: Icon(
        Icons.close,
        color: Colors.black,
      ),
      actions: <Widget>[],
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: new Center(
          child: column,
        ),
      ),
    );
  }
}
