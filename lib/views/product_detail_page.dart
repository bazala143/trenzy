import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';
import 'package:trenzy_ecommerce/widgets/image_view.dart';

class ProductDetailPage extends StatefulWidget {
  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  List<String> bannerList = new List();
  int _current = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bannerList.add("https://static.toiimg.com/photo/63393835/Vivo-Apex.jpg");
    bannerList.add("https://static.toiimg.com/photo/63393835/Vivo-Apex.jpg");
    bannerList.add("https://static.toiimg.com/photo/63393835/Vivo-Apex.jpg");
    bannerList.add("https://static.toiimg.com/photo/63393835/Vivo-Apex.jpg");
  }

  @override
  Widget build(BuildContext context) {

    var btnBack = Container(
      margin: EdgeInsets.only(left: 12.0,right: 12.0),
      child: Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(60.0),
        clipBehavior: Clip.antiAlias,
        child: ClipOval(
          child:InkWell(
            splashColor: Colors.grey, // inkwell color
            child: SizedBox(width: 30, height: 30, child: Icon(Icons.arrow_back_ios,size: 20.0)),
            onTap: () {},
          ),
        ),
      ),
    );

    var tvCartCount = Align(
      alignment: Alignment.topRight,
      child: Container(
        margin: EdgeInsets.only(right: 10.0),
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            color: Colors.black,
            shape: BoxShape.circle
        ),
        child: Text("20",style: TextStyle(
            color: Colors.white,
          fontSize: 10
        ),),
      ),
    );

    var btnWish = Container(
      child: Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(60.0),
        clipBehavior: Clip.antiAlias,
        child: ClipOval(
          child:InkWell(
            splashColor: Colors.grey, // inkwell color
            child: SizedBox(width: 30, height: 30, child: Icon(Icons.favorite_border,size: 20.0)),
            onTap: () {},
          ),
        ),
      ),
    );

    var btnCart = Container(
      child: Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(60.0),
        clipBehavior: Clip.antiAlias,
        child: ClipOval(
          child:InkWell(
            splashColor: Colors.grey, // inkwell color
            child: SizedBox(width: 30, height: 30, child: Icon(Icons.shopping_cart,size: 20.0)),
            onTap: () {},
          ),
        ),
      ),
    );

    var cartStack  = Container(
      height: 40,width: 50,child: Stack(
      alignment: Alignment.center,
      children: <Widget>[btnCart,tvCartCount],
    ),);

    var endBtnRow = new Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        cartStack,btnWish
      ],
    );

    var appBarRow = Container(
      margin: EdgeInsets.only(right: 15.0,top: 35.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[btnBack,endBtnRow],
      ),
    );
    var dots = Align(
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(bannerList.length, (index) {
          return new Container(
            height: 8,
            width: 15,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
                color: index == _current ? Colors.black54 : Colors.grey),
          );
        }),
      ),
    );
    var slider = CarouselSlider(
      viewportFraction: 1.0,
      aspectRatio: 2.0,
      autoPlay: false,
      enlargeCenterPage: false,
      onPageChanged: (index) {
        setState(() {
          _current = index;
        });
      },
      height: 300.0,
      items: List.generate(bannerList.length, (index) {
        return Container(
          alignment: Alignment.center,
          height: 300,
          child: new ProgressImageView(
            margin: EdgeInsets.only(left: 5.0,right: 5.0,top: 40),
            height: 200,
            scaleType: BoxFit.contain,
            width: double.infinity,
            imageurl: bannerList[index],
          ),
        );
      }),
    );

    var sliderStack = Container(
      height: 300,
      color: Colors.white,
      child: Stack(
        children: <Widget>[slider, dots,appBarRow],
      ),
    );

    var tvProductName = Align(
      alignment: Alignment.centerLeft,
      child: Container(
        width: 200,
        child: CTextView(
          margin: EdgeInsets.only(top: 10.0),
          text: "Short Middies for Children",
          fontWeight: FontWeight.bold,
          textSize: 20,
          textColor: Colors.black,

        ),
      ),
    );
    var tvPriceOff = Align(
      alignment: Alignment.centerLeft,
      child: CTextView(
        margin: EdgeInsets.only(top: 10.0),
        text: "50% off",
        fontWeight: FontWeight.bold,
        textSize: 12,
        textColor: Colors.green,
        textDecoration: TextDecoration.lineThrough,
      ),
    );

    var tvProductPrice = Align(
      alignment: Alignment.centerLeft,
      child: CTextView(
        margin: EdgeInsets.only(top: 5.0),
        text: "\$200",
        fontWeight: FontWeight.bold,
        textSize: 18,
        textColor: Colors.black,
      ),
    );

   var priceColumn = Column(
     children: <Widget>[tvPriceOff,tvProductPrice],
   );
    var priceRow = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[tvProductName,priceColumn],
    );

    var ratingBar = RatingBar(
      initialRating: 2.3,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemSize: 14,
      tapOnlyMode: false,
      ignoreGestures: true,
      itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.amber,
      ),
      onRatingUpdate: (rating) {
        print(rating);
      },
    );


    var lblRatingReviews = Align(
      alignment: Alignment.centerLeft,
      child: CTextView(
        margin: EdgeInsets.only(top: 20.0),
        text: "Rating & Reviews",
        textSize: 16,
        textAlign: TextAlign.start,
        textColor: Colors.black54,
        fontWeight: FontWeight.normal,
      ),
    );
    var lblColors = Align(
      alignment: Alignment.centerLeft,
      child: CTextView(
        margin: EdgeInsets.only(top: 20.0),
        text: "Available Ram",
        textSize: 16,
        textAlign: TextAlign.start,
        textColor: Colors.black54,
        fontWeight: FontWeight.normal,
      ),
    );
    var greenDot = Container(
      height: 8.0,
      width: 8.0,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.green
      ),
    );

    var tvDeliveryFree = Align(
      alignment: Alignment.centerLeft,
      child: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[greenDot,CTextView(
            margin: EdgeInsets.only(left: 10.0),
            text: "Free delivery available order above \$20",
            textSize: 15,
            textAlign: TextAlign.start,
            textColor: Colors.grey,
            fontWeight: FontWeight.normal,
          )],
        ),
      ),
    );
    var tvDeliveryCharge = Align(
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[greenDot,CTextView(
          margin: EdgeInsets.only(left: 10.0),
          text: "Delivery charges apply order below \$5",
          textSize: 15,
          textAlign: TextAlign.start,
          textColor: Colors.grey,
          fontWeight: FontWeight.normal,
        )],
      ),
    );

    var deliveryColumn = Container(
      margin: EdgeInsets.only(top: 20.0),
      child: Material(
        color: Colors.white,
        elevation: 2.0,
        borderRadius: BorderRadius.circular(5.0),
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              tvDeliveryCharge,tvDeliveryFree
            ],
          ),
        ),
      ),
    );
    var tvReviewCount = CTextView(
      margin: EdgeInsets.only(top: 5.0),
      text: "(3226 Rating & Reviews)",
      textSize: 12,
      textAlign: TextAlign.justify,
      textColor: Colors.grey,
      fontWeight: FontWeight.normal,
    );

    var colorListView = new Container(
      margin: EdgeInsets.only(top: 10.0),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      height: 30,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: 4,
          itemBuilder: (context, index) {
            return Align(
              alignment: Alignment.center,
              child: Container(
                margin: EdgeInsets.only(right: 10.0),
                alignment: Alignment.center,
                height: 30,
                width: 80,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30.0),
                  border: Border.all(color: Colors.grey,width: 1)
                ),
                child: CTextView(
                  text: "4 GB",
                  textColor: Colors.grey,
                  textSize: 12.0,
                ),
              ),
            );
          }),
    );


    var ratingRow = Align(
      alignment: Alignment.centerLeft,
      child: new Container(
        margin: EdgeInsets.only(top: 5.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: ratingBar,
            ),
            Flexible(
              child: tvReviewCount,
            )
          ],
        ),
      ),
    );

    var tDesc = Align(
      alignment: Alignment.centerLeft,
      child: CTextView(
        margin: EdgeInsets.only(top: 5.0),
        text: "Short Middies for Children",
        fontWeight: FontWeight.normal,
        textSize: 12,
        textColor: Colors.grey,
      ),
    );

    var descColumn  = Container(
      margin: EdgeInsets.only(top:12.0),
      child: Material(
        color: Colors.white,
        elevation: 5.0,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0)),
        child: Container(
          margin: EdgeInsets.only(top:12.0,left: 15.0,right: 15.0,bottom: 15.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              priceRow,
              tDesc,
              lblRatingReviews,
              ratingRow,
              lblColors,colorListView,deliveryColumn
            ],
          ),
        ),
      ),
    );

    new Container(
      margin: EdgeInsets.only(top: 10.0),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      height: 30,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: 4,
          itemBuilder: (context, index) {
            return Align(
              alignment: Alignment.center,
              child: Container(
                margin: EdgeInsets.only(right: 10.0),
                alignment: Alignment.center,
                height: 30,
                width: 80,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30.0),
                    border: Border.all(color: Colors.grey,width: 1)
                ),
                child: CTextView(
                  text: "4 GB",
                  textColor: Colors.grey,
                  textSize: 12.0,
                ),
              ),
            );
          }),
    );



    var scrollView = Container(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            sliderStack,descColumn
           
          ],
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: MediaQuery.removePadding(context: context, child: scrollView,removeTop: true,),
    );
  }
}
