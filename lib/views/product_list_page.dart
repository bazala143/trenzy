import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/listitems/product_item.dart';
import 'package:trenzy_ecommerce/shimmer_views/product_list_shimmers.dart';
import 'package:trenzy_ecommerce/widgets/c_scaffold.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';

class ProductListPage extends StatefulWidget {
  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    AppDimens appDimens = new AppDimens(context);
    appDimens.init();


    var chips = Container(
      height: 55,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: List.generate(10, (index){
          return Container(
            margin: EdgeInsets.only(left: 5.0),
            child: Chip(
              backgroundColor: CommonColors.primaryColor,
              label: CTextView(
                text: "Skirts",
                textColor: Colors.white,
                textSize: 12,
              )),) ;
        }),
      ),
    );

    var gridView = GridView.count(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      padding: EdgeInsets.only(
          top: AppDimens.listVertcalPadding,
          left: AppDimens.listHorizontalPadding,
          right: AppDimens.listHorizontalPadding),
      crossAxisCount: 2,
      childAspectRatio: 0.63,
      children: List.generate(50, (int index) {
        return isLoading ? ProductItemShimmer() : ProductItem();
      }),
    );


    var mainListview  = ListView(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      children: <Widget>[chips,gridView],
    );


    return CScaffold(
      actions: <Widget>[
        Icon(
          Icons.shopping_cart,
          color: Colors.black,
        ),
        new Container(
          margin: EdgeInsets.only(right: 10.0, left: 10.0),
          child: Icon(
            Icons.search,
            color: Colors.black,
          ),
        )
      ],
      title: "Product List",
      isLeading: true,
      body: mainListview,
    );
  }
}
