import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/widgets/circule_imageview.dart';
import 'package:trenzy_ecommerce/widgets/c_scaffold.dart';
import 'package:trenzy_ecommerce/widgets/c_textview.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    var profileImageView = Align(
      alignment: Alignment.center,
      child: CircleImageView(
        height: 100,
        width: 100,
        imageUrl:
            "https://static-ssl.businessinsider.com/image/59f8dc483e9d25db458b5dfc-2400/gettyimages-645671866.jpg",
      ),
    );

    var icCamera = Align(
      alignment: Alignment.bottomRight,
      child: new Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        child:  new Container(
          padding: EdgeInsets.all(5.0),
          child: Icon(
            Icons.camera_alt,
            color: Colors.black,
            size: 20,
          ),
        ),
      ),
    );

    var tvUserName = Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: CTextView(
          text: "Elon musk ",
          textSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );

    var tvEmail = new Align(
      alignment: Alignment.center,
      child: new Container(
        margin: EdgeInsets.only(top: 5.0),
        child: CTextView(
          text: "elon@gmail.com",
          textSize: 15,
          textColor: Colors.grey,
        ),
      ),
    );

    var profileStack = Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: 20.0),
        height: 100,
        width: 100,
        child: Stack(
          children: <Widget>[profileImageView, icCamera],
        ),
      ),
    );



    var listview = ListView(
      shrinkWrap: true,
      children: <Widget>[profileStack, tvUserName, tvEmail,dividerWidget(Icons.bookmark_border,"Orders"),dividerWidget(Icons.home,"Address"),dividerWidget(Icons.exit_to_app,"Logout")],
    );

    return Scaffold(
      body: listview,
    );
  }


  Widget  dividerWidget(IconData iconData,String title){
    var tvName = CTextView(
      margin: EdgeInsets.only(left: 10.0),
      text:title,
      textSize: 15,
      textColor: Colors.grey,
    );

    var iconPre  = new Container(
      child:Icon(iconData,color: Colors.grey,) ,
    );
    var iconNext = Expanded(child: Align(
      alignment: Alignment.centerRight,
      child: Icon(Icons.navigate_next,color: Colors.grey,),
    ));

   var row = new Container(
     margin: EdgeInsets.only(top: 10.0,left: 10.0,right: 10.0),
     child: new Material(
       elevation: 1.0,
       child: Container(
         padding: EdgeInsets.only(left: 10.0,right: 10.0,top: 10.0,bottom: 10.0),
         child: Row(
           children: <Widget>[
             iconPre, tvName,iconNext
           ],
         ),
       ),
     ),
   );

   return  row;
  }
}
