import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/listitems/order_list_items.dart';

class UserOrderList extends StatefulWidget {
  @override
  _UserOrderListState createState() => _UserOrderListState();
}

class _UserOrderListState extends State<UserOrderList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 3,
        itemBuilder: (context,index){
       return Container(
         color: Colors.grey,
         child: OrderListItems(),);
    });
  }
}

