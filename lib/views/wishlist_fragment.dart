import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'package:trenzy_ecommerce/listitems/wishlist_items.dart';

class WishListFragment extends StatefulWidget {
  @override
  _WishListFragmentState createState() => _WishListFragmentState();
}

class _WishListFragmentState extends State<WishListFragment> {
  @override
  Widget build(BuildContext context) {

    AppDimens appDimens = new AppDimens(context);
    appDimens.init();
    var cartListview = AnimationLimiter(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: 2,
        itemBuilder: (BuildContext context, int index) {
          return AnimationConfiguration.staggeredList(
            position: index,
            duration: const Duration(milliseconds: 375),
            child: SlideAnimation(
              verticalOffset: 50.0,
              child: FadeInAnimation(
                child: WishlistItems(),
              ),
            ),
          );
        },
      ),
    );
    return Scaffold(
      body: cartListview ,
    );
  }
}
