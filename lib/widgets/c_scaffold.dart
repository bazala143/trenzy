import 'dart:io';

import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/app_constant.dart';

import 'c_textview.dart';

class CScaffold extends StatelessWidget {
  bool isLeading;
  List<Widget> actions;
  IconButton backButton;
  Function onBackPressed;
  Widget body;
  double elevation;
  String title;
  Widget leadingIcon;
  Widget bottomNavBar;
  Widget drawer;
  Color backgroundColor;

  CScaffold(
      {this.isLeading,
      this.actions,
      this.backButton,
      this.onBackPressed,
      this.body,
      this.elevation,
      this.title,
      this.leadingIcon,
      this.bottomNavBar,
      this.drawer,this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    var tvTitle = new CTextView(
      text: title ?? "",
      backgroundColor: Colors.transparent,
      fontWeight: FontWeight.bold,
      fontFamily: AppConstants.FONT_FAMILY,
      textColor: Colors.black,
      textSize: 20,
    );

    var appbar = PreferredSize(
      child: AppBar(
        actions: actions ??
            <Widget>[
              new Container(
                height: 1,
                width: 1,
              )
            ],
        elevation: elevation ?? 2.0,
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: leadingIcon ??
            Builder(
              builder: (BuildContext context) {
                return IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(
                    Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                    color: Colors.black,
                  ),
                  alignment: Alignment(-1, 0.0), // move icon a bit to the left
                );
              },
            ),
        automaticallyImplyLeading: false,
        title: tvTitle,
      ),
      preferredSize: Size.fromHeight(45),
    );
    return Scaffold(
      backgroundColor: backgroundColor ?? Colors.white,
      drawer: drawer ?? null,
      bottomNavigationBar: bottomNavBar == null
          ? new Container(
              height: 1,
              width: 1,
            )
          : new Material(
              elevation: 5,
              child: new Container(
                padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: bottomNavBar,
              )),
      appBar: appbar,
      body: body,
    );
  }
}
