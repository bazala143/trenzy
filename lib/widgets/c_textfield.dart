import 'package:flutter/material.dart';

class CTextField extends StatelessWidget {
  final TextEditingController textEditingController;
  final String hint;
  final bool enabled;
  final FocusNode focusNode;
  final int maxLength, maxLines;
  final Function(String) validator;
  final TextInputAction textInputAction;
  final TextAlign textAlign;
  final bool enableSuggestions;
  final Function(String) onChanged;
  final EdgeInsets margin;
  final bool obscureText;
  final Function onShowHide;

  CTextField(
      {this.textEditingController,
      this.hint,
      this.enabled,
      this.focusNode,
      this.maxLength,
      this.maxLines,
      this.validator,
      this.textInputAction,
      this.textAlign,
      this.enableSuggestions,
      this.onChanged,
      this.margin,this.obscureText,this.onShowHide});

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: margin ?? EdgeInsets.all(0.0),
      child: TextFormField(
        obscureText: obscureText ?? false ,
        controller: textEditingController,
        focusNode: focusNode ?? new FocusNode(),
        enabled: enabled == null ?? true,
        maxLength: maxLength ?? null,
        maxLines: maxLines ?? null,
        validator: validator ??
            (value) {
              if (value.isEmpty) {
                return "Not Should be empty";
              }
            },
        textInputAction: textInputAction ?? TextInputAction.done,
        textAlign: textAlign ?? TextAlign.start,
        enableSuggestions: enableSuggestions ?? false,
        onChanged: onChanged ?? (value) {},
        decoration:
            InputDecoration(
                suffixIcon: obscureText != null ? InkWell(child: Icon(Icons.remove_red_eye),onTap: onShowHide ?? (){} ,) : null,
                labelText: hint, border: UnderlineInputBorder()),
      ),
    );
  }
}
