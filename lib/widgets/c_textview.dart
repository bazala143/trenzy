import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/app_constant.dart';

class CTextView extends StatelessWidget {
  final EdgeInsets margin;
  final EdgeInsets padding;
  final String text;
  final TextAlign textAlign;
  final FontWeight fontWeight;
  final double textSize;
  final Color backgroundColor;
  final Color textColor;
  final String fontFamily;
  final int maxLines;
  final TextOverflow textOverflow;
  final TextDecoration textDecoration;


  CTextView(
      {this.margin,
      this.padding,
      this.text,
      this.textAlign,
      this.fontWeight,
      this.textSize,
      this.backgroundColor,
      this.textColor,
      this.fontFamily,
      this.maxLines,
      this.textOverflow,this.textDecoration});

  @override
  Widget build(BuildContext context) {
    return new Container(
      color: backgroundColor == null ? Colors.transparent : backgroundColor,
      margin: margin == null ? EdgeInsets.all(0.0) : margin,
      padding: padding == null ? EdgeInsets.all(0.0) : padding,
      child: new Text(
        text,
        textAlign: textAlign == null ? TextAlign.start : textAlign,
        maxLines: maxLines == null ? 100 : maxLines,
        overflow: textOverflow == null ? TextOverflow.ellipsis : textOverflow,
        style: TextStyle(
          decoration: textDecoration ?? TextDecoration.none,
            color: textColor == null ? Colors.black : textColor,
            fontFamily:
                fontFamily == null ? AppConstants.FONT_FAMILY : fontFamily,
            fontSize: textSize == null ? 10.0 : textSize,
            fontWeight: fontWeight == null ? FontWeight.normal : fontWeight),
      ),
    );
    ;
  }
}
