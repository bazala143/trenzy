import 'dart:io';
import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';

class CircleImageView extends StatelessWidget {
  final String imageUrl;
  final EdgeInsets margin;
  final double borderWidth;
  final Color borderColor;
  final double height;
  final double width;
  final double borderRadious;
  final Color backgroundColor;

  CircleImageView(
      {this.imageUrl,
      this.margin,
      this.borderWidth,
      this.borderColor,
      this.height,
      this.width,this.backgroundColor,this.borderRadious});

  @override
  Widget build(BuildContext context) {
    final networkImage = NetworkImage(imageUrl);

    final fileImage = FileImage(new File(imageUrl));

    // TODO: implement build
    return new Container(
        height: height == null ? 70 : height,
        width: width == null ? 70 : width,
        margin: margin == null ? EdgeInsets.all(0.0) : margin,
        decoration: BoxDecoration(
             color: CommonColors.grabck,
            borderRadius: BorderRadius.circular(borderRadious == null ? 60.0 : borderRadious),
            border: Border.all(
                color: borderColor == null
                    ? CommonColors.primaryColor
                    : borderColor,
                width: borderWidth == null ? 2.0 : borderWidth)),
//        child: imageUrl.startsWith("http") ? networkImage : localImage);
        child: CircleAvatar(
          radius: 30.0,
          backgroundImage:
              imageUrl.startsWith("http") ? networkImage : fileImage,
          backgroundColor: backgroundColor == null ? Colors.transparent : backgroundColor,
        ));
  }
}
