import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trenzy_ecommerce/common/common_colors.dart';
import 'package:trenzy_ecommerce/common/app_dimens.dart';
import 'c_textview.dart';

class RectButton extends StatelessWidget {
  final Function onPressed;
  final String   btnText;
  final backgroundColor;
  final double borderWidth;
  final double borderColor;
  final double borderRadious;
  final double elevation;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double height,width;
  final double btnTextsize;
  final Color btnTextColor;


  RectButton({this.onPressed, this.btnText, this.backgroundColor,
      this.borderWidth, this.borderColor, this.borderRadious, this.elevation,this.margin,this.padding,this.height,this.width,this.btnTextsize,this.btnTextColor});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed ?? (){},
      child: new Container(
        margin: margin ?? EdgeInsets.all(0.0) ,
        child: new Material(
          elevation: elevation ?? 0,
          borderRadius: BorderRadius.circular(borderRadious ?? 0),
          child: new Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: backgroundColor ?? CommonColors.primaryBlue,
              border: Border.all(color: borderColor ?? Colors.transparent,width: borderWidth ?? 0),
              borderRadius: BorderRadius.circular(borderRadious ?? 0)
            ),
            height: height ?? ScreenUtil().setHeight(45),
            width: width ?? MediaQuery.of(context).size.width - 20,
            padding: padding ?? EdgeInsets.all(0),
            child: CTextView(
              textSize: AppDimens.fontSizeBig,
              text:  btnText ?? "Button",
              textColor: btnTextColor ?? Colors.white,
            ),
          ),
        ),

      ),
    );
  }
}
