import 'dart:io';

import 'package:flutter/material.dart';
import 'package:trenzy_ecommerce/common/image_path.dart';

class ProgressImageView extends StatelessWidget {
  final String imageurl;
  final double height;
  final double width;
  final EdgeInsets margin;
  final double borderWidth;
  final Color borderColor;
  final BoxFit scaleType;
  double borderRadius;
  bool isCircule;
  final String imagePath;
  final double elevation;
  final Color shadowColor;

  ProgressImageView(
      {this.imageurl,
      this.height,
      this.width,
      this.margin,
      this.borderWidth,
      this.borderColor,
      this.scaleType,
      this.isCircule,
      this.borderRadius,
      this.imagePath,
      this.elevation,
      this.shadowColor});

  @override
  Widget build(BuildContext context) {
    if (isCircule == null) {
      isCircule = false;
    }



    if (isCircule) {
      borderRadius = 60;
    } else {
      borderRadius = borderRadius == null ? 0.0 : borderRadius;
    }
    final fadeInImage = new FadeInImage.assetNetwork(
        fit: scaleType == null ? BoxFit.cover : scaleType,
        placeholder: ImagePath.ic_placeholder,
        placeholderScale:0.2,
        placeholderCacheHeight: 20,
        placeholderCacheWidth: 20,
        image: imageurl == null ? "" : imageurl);

    final fileImage = FileImage(new File(imageurl));

    final imageContainer = new Container(
      child: new ClipRRect(
        child: imageurl.startsWith("http") ? fadeInImage : fileImage,
        borderRadius: new BorderRadius.circular(borderRadius),
      ),
      height: height == null ? 0 : height,
      width: width == null ? 0 : width,
      margin: margin == null ? EdgeInsets.all(0.0) : margin,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(isCircule ? 60.0 : (borderRadius)),
          border: Border.all(
              color:
                  borderColor == null ? Colors.transparent : borderColor,
              width: borderWidth == null ? 0.0 : borderWidth)),
    );
    return imageContainer;
  }
}
