import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  final String hint;
  final Widget prefixIcon;
  final Color backgroundColor;
  final int maxLines;
  final double borderRadious,borderWidth,elevation;
  final Color borderColor;
  final Function onTap,onChange;
  final TextEditingController controller;
  final bool isEnabled;
  final TextInputAction  textInputAction;


  SearchField({@required this.hint, this.prefixIcon, this.backgroundColor, this.maxLines,
      this.borderRadious, this.borderWidth, this.elevation, this.borderColor,
      this.onTap, this.onChange, @required this.controller, this.isEnabled,
      this.textInputAction});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChange,
      controller: controller ,
      enabled: isEnabled ?? true,
      maxLines: 1,
      decoration: InputDecoration(
        prefixIcon: prefixIcon ?? null,
        fillColor: backgroundColor ??  Colors.white,
        hintText: hint,
          border: OutlineInputBorder(
          borderRadius: borderRadious == null ? BorderRadius.circular(0.0) : BorderRadius.circular(borderRadious),
          borderSide: BorderSide(color: borderColor ?? Colors.transparent)
        )
      ),
    );
  }
}
